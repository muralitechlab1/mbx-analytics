package org.mbx.analytics.server.controllers;

import org.json.simple.JSONObject;
import org.mbx.analytics.server.services.ReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/reports")
public class ReportController {

    @Autowired
    private ReportService reportService;

    @Value("${host}")
    private String HOST;

    @RequestMapping(method = RequestMethod.GET)
    public String getReports() {
        try {

            return "Welcome";

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return "failure";
    }



    @RequestMapping(method = RequestMethod.POST)
    public String postTelemetry(@RequestBody JSONObject jsonObject) {
        try {

            return reportService.processRequest(HOST,jsonObject);

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return "failure";
    }


}
