package org.mbx.analytics.server.services;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;


@Service
public class ReportService {

    @Value("${deviceAccessToken}")
    private String DEVICE_ACCESS_TOKEN;

    @Value("${accessToken}")
    private String ACCESS_TOKEN;

    @Value("${deviceTelemetry}")
    private String TELEMETRY_ENDPOINT;


    public String processRequest(String host, JSONObject jsonObject){


        //
        JSONObject telemetryResp = doAnalyticsRequest(jsonObject);

        return sendAnalyticsResponseAsTelemetry(host,telemetryResp);

    }

    private String sendAnalyticsResponseAsTelemetry(String host, JSONObject telemetryResponse) {

        String endpoint = getPublishEndpoint(host, DEVICE_ACCESS_TOKEN);

        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders requestHeaders = null;
        if (telemetryResponse != null) {
            if (ACCESS_TOKEN != null)
                requestHeaders = getHttpHeaders(ACCESS_TOKEN);

            HttpEntity<?> httpEntity = new HttpEntity<Object>(telemetryResponse, requestHeaders);
            ResponseEntity<String> response = restTemplate.postForEntity(endpoint,
                    httpEntity, String.class);

            HttpStatus status = response.getStatusCode();
            String res = response.getBody();
            return res;
        }

        return "Success";
    }

    private JSONObject doAnalyticsRequest(JSONObject jsonObject) {

        return jsonObject;
    }

    private HttpHeaders getHttpHeaders(String accessToken) {
        HttpHeaders requestHeaders = new HttpHeaders();
        requestHeaders.add("Authorization", "Bearer " + ACCESS_TOKEN);
        return requestHeaders;
    }

    private String getPublishEndpoint(String host,String deviceToken) {
        return host+TELEMETRY_ENDPOINT.replace("DEVICE_TOKEN", deviceToken);
    }
}
